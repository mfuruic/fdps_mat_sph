#pragma once

template <class Ptcl> class Dam: public Problem<Ptcl>{
	public:
	static const double END_TIME = 1;
	static void setupIC(PS::ParticleSystem<Ptcl>& sph_system, system_t& sysinfo, PS::DomainInfo& dinfo){
		/////////
		//place ptcls
		/////////
		std::vector<Ptcl> ptcl;
		const PS::F64 box_x =0.5;
		const PS::F64 box_y =0.5;
		const PS::F64 box_z = 0.25;

		const PS::F64 dx = box_x / 32.0;
		PS::S32 cnt = 0;
		for(PS::F64 x = 0 ; x < box_x ; x += dx){
			for(PS::F64 y = 0 ; y < box_y ; y += dx){
				for (PS::F64 z = 0; z < box_z; z += dx) {

					++cnt;
					Ptcl ith;
					ith.type = HYDRO;
					ith.tag = 0;
					if (y < 0.1 * box_y || x < box_x * 0.1 || z < box_z * 0.2 ) {
						ith.type = FREEZE;
						ith.tag = 1;
					}
					else if (y < 3. / 4. * box_y && x < 0.5 * box_x) {
					}
					else {
						continue;
					}
					ith.pos.x = x;
					ith.pos.y = y;
					ith.pos.z = z;
					ith.dens = EoS::Water.ReferenceDensity();
					ith.mass = ith.dens * box_x * box_y* box_z;
					ith.id = cnt;
					ith.EoS = &EoS::Water;
					ith.visc = &Viscosity::Water;
					ptcl.push_back(ith);
				}
			}
		}
		for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
			ptcl[i].mass = ptcl[i].dens * box_x * box_y * box_z / (PS::F64)(cnt);
		}
		std::cout << "# of ptcls is... " << ptcl.size() << std::endl;
		//
		dinfo.setBoundaryCondition(PS::BOUNDARY_CONDITION_PERIODIC_XZ);
		dinfo.setPosRootDomain(PS::F64vec(0.0, 0.0, 0.0), PS::F64vec(box_x, box_y, box_z));
		if(PS::Comm::getRank() == 0){
			const PS::S32 numPtclLocal = ptcl.size();
			sph_system.setNumberOfParticleLocal(numPtclLocal);
			for(PS::U32 i = 0 ; i < ptcl.size() ; ++ i){
				sph_system[i] = ptcl[i];
			}
		}else{
			sph_system.setNumberOfParticleLocal(0);
		}
		//Fin.
		std::cout << "setup..." << std::endl;
	}
	static void addExternalForce(PS::ParticleSystem<Ptcl>& sph_system, system_t& system){
		for(PS::U64 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
			sph_system[i].acc.y -= 9.8;
		}
	}
};

