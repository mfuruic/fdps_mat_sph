#pragma once

namespace PARAM{
	const PS::F64 SMTH = 1.0;
	const PS::F64 C_CFL = 0.3;
	const PS::U64 NUMBER_OF_SNAPSHOTS = 100;
	//const PS::U64 NUMBER_OF_DENSITY_SMOOTHING_LENGTH_LOOP = 3;
	//Balsara (1995)'s switch
	const bool FLAG_B95  = true;
	//Rosswog et al. (2000)'s switch
	const bool FLAG_R00 = false;
	//AV STRENGTH
	const double AV_STRENGTH = 1.0;
};

