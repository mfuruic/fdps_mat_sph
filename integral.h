#pragma once
template <class ThisPtcl> PS::F64 getTimeStepGlobal(const PS::ParticleSystem<ThisPtcl>& sph_system){
	PS::F64 dt = 1.0e+30;//set VERY LARGE VALUE
	PS::F64 SOUND_VEL0 = 20. * sqrt(9.8*0.5);
	PS::F64 dx = 0.5 / 32.0;
	/*
	for(PS::S32 i = 0 ; i < sph_system.getNumberOfParticleLocal() ; ++ i){
		PS::F64 dt_tmp = 1.0e+30;

		dt_tmp = std::min(sqrt(sph_system[i].smth / sqrt(sph_system[i].acc * sph_system[i].acc)), dt_tmp);
		if(sph_system[i].type != FREEZE){
			dt = std::min(dt, std::min(sph_system[i].dt, PARAM::C_CFL * dt_tmp));
		}else{
			dt = std::min(dt, PARAM::C_CFL * dt_tmp);
		}
	}
	return PS::Comm::getMinValue(dt);
	*/
	return 0.05 * dx  / SOUND_VEL0 ;
}

