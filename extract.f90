program main
implicit none
real(8),save::C_rad= 0.125d-3
Integer(4),parameter :: isize=2,istart=2,iend=2
INTEGER(4) :: istep_gout,irank,gPnum_dom_dem(isize),ICN(istart:iend),gPnum_dom_sph(isize)


INTEGER(8),save :: gPnum_total=0,Init_p
integer ::ii,i,it,idx,idy,idz,ix,iy,iz,ijk,is,j,k
REAL(4),DIMENSION(:,:),allocatable :: GPOS,GPOS2
INTEGER(8),DIMENSION(:),allocatable :: IGPOS
REAL(4),DIMENSION(:),allocatable :: RPOS
REAL(4),DIMENSION(:),allocatable ::vx,vy,vz
REAL(4),DIMENSION(:),allocatable :: SCA1,sca2,GSCA
INTEGER(4),DIMENSION(:),allocatable :: label,col_p,IGPOS2
REAL(4),DIMENSION(:),allocatable :: posx,posy,posz,rad
CHARACTER*5 ::cMyrank
CHARACTER*5 ::cStep
real(4)::minx,maxx,miny,maxy,minz,maxz
integer::i1,i2,i3,nx,ny,nz,ia,ja,ka,nn,totalp,ndem,nsph
integer,save :: it_total=-1
logical,allocatable::flg(:,:,:),flg_o(:,:,:)
real::dx,dy,dz 
real*8::tot_void,WA1

type path_
        CHARACTER*60 :: dir
end type path_
type(path_)PATH(istart:iend)




do is = istart,iend ! why need this ? mikito

!   path(is)%dir='../SPH_DEM/mound/test/old/'          
 path(is)%dir='/home/mikito/FDPS/src/result/'

!  path(is)%dir='./rboil1mm/'
!   path(is)%dir='../SPH_DEM/mound/analog_boil_noeplimit/'
!path(is)%dir='/S/data06/G4013/y0173/SPH_DEM/mound/real/data4_dt0.1/'

!    WRITE(path(is)%dir(42:),FMT='(I1.1)') is

    write(*,*)path(is)%dir
icn(is)=len_trim(path(is)%dir)







  
   

if(is==istart)then
       gPnum_total=0
    do irank =1,isize
        WRITE(cMyrank,FMT='(I5.5)') irank-1

 OPEN(120,FILE=PATH(is)%dir(1:ICN(is))//'p_pos'//cMyrank,FORM='UNFORMATTED',access='stream')
!READ(120)gPnum_dom_dem(irank)
!write(6,*)gPnum_dom_dem(irank)

!ALLOCATE(GPOS(3,gPnum_dom_dem(irank)))
! READ(120)GPOS
!write(6,*)GPOS(1:3,gPnum_dom_dem(irank))
!stop

!        OPEN(120,CONVERT="BIG_ENDIAN",FILE=PATH(is)%dir(1:ICN(is))//'p_pos'//cMyrank,FORM='UNFORMATTED')
 


!        OPEN(120,CONVERT="BIG_ENDIAN",FILE=PATH(is)%dir(1:ICN(is))//'p_chain'//cMyrank,FORM='UNFORMATTED')
!       OPEN(120,FILE=PATH(is)%dir(1:ICN(is))//'p_chain'//cMyrank,FORM='UNFORMATTED') 

       READ(120)gPnum_dom_dem(irank)
write(6,*)"fa",gPnum_dom_dem(irank)
        CLOSE(120)
        gPnum_total=gPnum_total+gPnum_dom_dem(irank) !Total particle number in the system
!write(6,*)"init  gPnum_total", gPnum_total
!--------------------------------
!gPnum_total=50000000
!----------------------------------
        write(6,*)"now at",irank,gPnum_dom_dem(irank)
    enddo
!stop
!   gPnum_total = 150000000
write(6,*)"Total particle number in the system",gPnum_total
  gPnum_total =  10000 !3000000
!gPnum_total =   50000
!   gPnum_total =120000000

   write(6,*)"Total particle number in the system",gPnum_total
   totalp=gPnum_total
   ALLOCATE(col_p(280000000)) 
    ALLOCATE(posx(gPnum_total),posy(gPnum_total),posz(gPnum_total),rad(gPnum_total),label(gPnum_total))
    allocate(sca1(gPnum_total),sca2(gPnum_total))
    ALLOCATE(vx(gPnum_total),vy(gPnum_total),vz(gPnum_total))
col_p=0
endif
!stop
!--------------------------------------






!--------------------------------------------------------------------   

    do irank =1,isize
        WRITE(cMyrank,FMT='(I5.5)') irank-1

!        OPEN(irank+2000,CONVERT="BIG_ENDIAN",FILE=PATH(is)%dir(1:ICN(is))//'p_pos'//cMyrank,FORM='UNFORMATTED')
        OPEN(irank+2000,FILE=PATH(is)%dir(1:ICN(is))//'p_pos'//cMyrank,FORM='UNFORMATTED',access='stream')

    enddo       
istep_gout=100000
!--------------------------------------------------------------------   
    do it =1,istep_gout
        it_total=it_total+1
        gPnum_total=0  

write(6,*)"step",it,"start 3rd part"
            ndem=0;nsph=0
            tot_void=0.0d0
        do irank =1,isize
!            READ(irank+20)gPnum_dom_dem(irank)
           READ(irank+2000)gPnum_dom_dem(irank)
           ALLOCATE(GPOS(3,gPnum_dom_dem(irank)))
!            allocate(GPOS2(4,gPnum_dom_dem(irank)),IGPOS(gPnum_dom_dem(irank)))
write(6,*)gPnum_dom_dem(irank)
!            READ(irank+20)GPOS





!           READ(irank+2000)IGPOS
           READ(irank+2000)GPOS
           if(it.eq.1)then
do i=1,gPnum_dom_dem(irank)
!if(GPOS2(2,i).le.0.0025)
!col_p(IGPOS(i))=int(GPOS2(2,i)/0.0025)+1
!write(6,*)IGPOS(i),col_p(IGPOS(i))
enddo
           endif

maxx=0.0
minx=100.0
           do i=1,gPnum_dom_dem(irank)
!WA1=1.0d0*GPOS(4,i)

            gPnum_total=gPnum_total+1
            posx(gPnum_total)=GPOS(1,i)
            posy(gPnum_total)= GPOS(2,i)
            posz(gPnum_total)=GPOS(3,i)
 !           vx(gPnum_total)=GPOS(4,i)
 !           vy(gPnum_total)=GPOS(5,i)
 !           vz(gPnum_total)=GPOS(6,i)
!            sca1(gPnum_total)=GPOS2(4,i)
!            label(gPnum_total)=col_p(IGPOS(i))

!write(6,*)real(GPOS(1:3,i))
ndem=ndem+1
           enddo

!write(6,*)maxx,minx



            DEALLOCATE(GPOS)
        enddo          
1234 continue   
write(6,*)"DEM num",ndem
do i=gPnum_total+1,totalp
            posx(i)=0
            posy(i)= 0
            posz(i)=0

enddo
!stop    
    WRITE(cStep,FMT='(I5.5)') it_total


if(it.eq.1)then

    OPEN(7,FILE='./sppx'//cStep//'.dat',Form='unformatted')
    OPEN(8,FILE='./sppy'//cStep//'.dat',Form='unformatted')
    OPEN(9,FILE='./sppz'//cStep//'.dat',Form='unformatted')
!    OPEN(10,FILE='./pn'//cStep//'.dat',Form='unformatted')
!    OPEN(11,FILE='./sppr'//cStep//'.dat',Form='unformatted')
!    OPEN(12,FILE='./scol'//cStep//'.dat',Form='unformatted')
!    OPEN(13,FILE='./pq'//cStep//'.dat',Form='unformatted')
!    OPEN(14,FILE='./pvx'//cStep//'.dat',Form='unformatted')
!    OPEN(15,FILE='./pvy'//cStep//'.dat',Form='unformatted')
!    OPEN(16,FILE='./pvz'//cStep//'.dat',Form='unformatted')
!    OPEN(17,FILE='./dumy'//cStep//'.dat',Form='unformatted')

endif

!if(mod(it,10).eq.1.and. it.le.10000)then
!if(mod(it,20).eq.1.and. it.le.10000)then
!if(it.le.10)then
!
!if(it.eq.40)then
!    write(6,*)"gPnum_total ",gPnum_total,ndem,it
!if(mod(it,2).eq.1.and. it.le.1000000)then

write(6,*)"gPnum_total ",gPnum_total,ndem,it, tot_void/nsph
!    write(7)gPnum_total
!    write(8)gPnum_total
!    write(9)gPnum_total
!    write(10)gPnum_total
!    write(11)gPnum_total
!if(gPnum_total.eq.0) cycle
if(ndem.eq.0)cycle
    write(7)posx(1:totalp)
    write(8)posy(1:totalp)
    write(9)posz(1:totalp)
!    write(10)label(1:gPnum_total)
!    vx=label
!    write(11)sca1(1:totalp)
!    write(12)vx(1:totalp)
 !   write(13)sca2(1:totalp)
!    write(14)vx(1:totalp)
!    write(15)vy(1:totalp)
!    write(16)vz(1:totalp)
!vx=0.0
!    write(17)sca1(1:totalp)
!endif
!    close(10)
!    close(11)
!    close(9)
!    close(8)
!    close(7)   
    enddo   



    do irank =1,isize
        WRITE(cMyrank,FMT='(I5.5)') irank-1
!        CLOSE(irank+20)
   CLOSE(irank+2000)
    enddo
   


enddo

end program
