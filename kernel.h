#pragma once

//Wendland C6
struct WendlandC6{
	//const PS::F64 pi = atan(1.0) * 4.0;
	static const PS::F64 pi = M_PI;
	PS::F64 plus(const PS::F64 arg) const{
		return (arg > 0) ? arg : 0;
	}
	PS::F64 pow3(const PS::F64 arg) const {
		const PS::F64 arg3 = arg * arg * arg;
		return arg3;
	}
	PS::F64 pow7(const PS::F64 arg) const{
		const PS::F64 arg3 = arg * arg * arg;
		return arg3 * arg3 * arg;
	}
	PS::F64 pow8(const PS::F64 arg) const{
		const PS::F64 arg2 = arg * arg;
		const PS::F64 arg4 = arg2 * arg2;
		return arg4 * arg4;
	}
	WendlandC6(){}
	//W
	PS::F64 W(const PS::F64vec dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
		r_value = (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))) * pow8(plus(1.0 - s));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= (78./7.) / (H * H * pi);
		#else
		r_value *= (1365./64.) / (H * H * H * pi);
		#endif
		return r_value;
	}
	/*
	//W
	PS::F64 W(const PS::F64 dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = dr / H;
		PS::F64 r_value;
		r_value = (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))) * pow8(plus(1.0 - s));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= (78./7.) / (H * H * pi);
		#else
		r_value *= (1365./64.) / (H * H * H * pi);
		#endif
		return r_value;
	}
	*/
	//gradW
	PS::F64vec gradW(const PS::F64vec dr, const PS::F64 h) const{
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
		r_value = pow7(plus(1.0 - s)) * (plus(1.0 - s) * (8.0 + s * (50.0 + s * (96.0))) - 8.0 * (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))));
		#ifdef PARTICLE_SIMULATOR_TWO_DIMENSION
		r_value *= (78./7.) / (H * H * pi);
		#else
		r_value *= (1365./64.) / (H * H * H * pi);
		#endif
		return dr * r_value / (sqrt(dr * dr) * H  + 1.0e-6 * h);
	}
	static PS::F64 supportRadius(){
		//return 3.5;
		return 2.1;
	}
};




//Wendland C2
struct WendlandC2 {

	static const PS::F64 pi = M_PI;
	PS::F64 plus(const PS::F64 arg) const {
		return (arg > 0) ? arg : 0;
	}
	PS::F64 pow3(const PS::F64 arg) const {
		const PS::F64 arg3 = arg * arg * arg;
		return arg3;
	}
	PS::F64 pow7(const PS::F64 arg) const {
		const PS::F64 arg3 = arg * arg * arg;
		return arg3 * arg3 * arg;
	}
	PS::F64 pow8(const PS::F64 arg) const {
		const PS::F64 arg2 = arg * arg;
		const PS::F64 arg4 = arg2 * arg2;
		return arg4 * arg4;
	}
	WendlandC2() {}
	//W
	PS::F64 W(const PS::F64vec dr, const PS::F64 h) const {
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
		r_value = (1.0 + 4*s ) * pow3(plus(1.0 - s))* plus(1.0 - s);

//		r_value = (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))) * pow8(plus(1.0 - s));
		r_value *= (21. / 2.) / (H * H * H * pi);
		return r_value;
	}
	//gradW
	PS::F64vec gradW(const PS::F64vec dr, const PS::F64 h) const {
		const PS::F64 H = supportRadius() * h;
		const PS::F64 s = sqrt(dr * dr) / H;
		PS::F64 r_value;
//		r_value =  pow3(plus(1.0 - s))*(4. * (1 - s) - 4. -16. * s);
		r_value = pow3(plus(1.0 - s))*(- 20. * s);

//		r_value = pow7(plus(1.0 - s)) * (plus(1.0 - s) * (8.0 + s * (50.0 + s * (96.0))) - 8.0 * (1.0 + s * (8.0 + s * (25.0 + s * (32.0)))));
//		r_value *= (1365. / 64.) / (H * H * H * pi);
//		r_value *= (105. * 2.) / (H * H * H * H * H * pi);
		r_value *= (21. / 2.) / (H * H * H * pi);

		return dr * r_value / (sqrt(dr * dr) * H + 1.0e-6 * h);
	}
	static PS::F64 supportRadius() {
		return 2.1;
	}
};

typedef WendlandC2 kernel_t;

